public class MatrixCalculator {
	public static void main(String[] args) {
		int[][] matrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};
		int[][] matrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};
		int[][] sum = add(matrixA, matrixB);
		for (int i = 0; i < sum.length; i++) {
			int[] element = sum[i];
			for (int j = 0; j < element.length; j++)
				System.out.print(element[j] + " ");
			System.out.println();
		}
	}

	public static int[][] add(int[][] matrixA, int[][] matrixB) {
		int[][] result = new int[3][3];
		for (int i = 0; i < 3; i++) {
			int[] matrixArow = matrixA[i];
			int[] matrixBrow = matrixB[i];
			int[] resultRow = result[i];
			for (int j = 0; j < 3; j++) {
				resultRow[j] = matrixArow[j] + matrixBrow[j];
			}
			System.out.println();
			
		}
		return result;
	}
}
