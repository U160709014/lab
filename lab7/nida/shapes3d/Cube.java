package nida.shapes3d;
import nida.shapes.Square; 

public class Cube extends Square {
	public Cube() {
		super(5);
		System.out.println("Cube is being create");
	}
	public int area() {
		return 6* super.area();
	}
	public double volume() {
		return super.area() * side ;
	}
	public String toString() {
		return "side = " + side ;
	}

}
